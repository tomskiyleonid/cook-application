package ru.savushkin.cookapp.modules.list_recipes

import ru.savushkin.cookapp.data.models.Recipe

interface IListRecipesView {
    fun showRecipes(recipes: ArrayList<Recipe>)
    fun navigateToRecipeDetails(recipeId: String)
}

interface IListRecipesPresenter {
    fun onViewAttached()
    fun onRecipeItemClick(recipe: Recipe)
}