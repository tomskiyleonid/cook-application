package ru.savushkin.cookapp.modules.recipe_details

import ru.savushkin.cookapp.data.models.RecipeDetails

interface IDetailsRecipeView {
    fun showRecipeDetails(data: RecipeDetails)
    fun showWrongFetchToast(message: String)
}

interface IDetailsRecipePresenter {
    fun onViewAttached(uuid: String)
}