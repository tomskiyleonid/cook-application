package ru.savushkin.cookapp.modules.list_recipes

import android.util.Log
import ru.savushkin.cookapp.data.api.ApiCallback
import ru.savushkin.cookapp.data.models.Recipe
import ru.savushkin.cookapp.data.models.ListRecipes
import ru.savushkin.cookapp.data.repositories.RecipesRepository

class ListRecipesPresenter(val view: IListRecipesView) :
    IListRecipesPresenter {

    private val recipesRepository =
        RecipesRepository()

    override fun onViewAttached() {
        getRecipes()
    }

    private fun getRecipes() {
        recipesRepository.fetchRecipeList(object :
            ApiCallback<ListRecipes> {
            override fun onSuccess(data: ListRecipes) {
                view.showRecipes(recipes = data.recipes)
            }

            override fun onError(message: String) {
                Log.e("Fetch recipes Error", message)
            }
        })
    }

    override fun onRecipeItemClick(recipe: Recipe) {
        view.navigateToRecipeDetails(recipeId = recipe.uuid)
    }
}