package ru.savushkin.cookapp.data.models

class RecipeBrief (
    val uuid: String,
    val name: String
)