package ru.savushkin.cookapp.data.services

import ru.savushkin.cookapp.data.models.ListRecipeDetails
import ru.savushkin.cookapp.data.models.ListRecipes
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface RecipeService {
    @GET("/recipes")
    fun fetchListRecipes(): Call<ListRecipes>

    @GET("/recipes/{uuid}")
    fun fetchRecipeById(@Path("uuid") recipeId: String?): Call<ListRecipeDetails>

}
