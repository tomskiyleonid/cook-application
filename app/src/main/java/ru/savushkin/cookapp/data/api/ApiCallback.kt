package ru.savushkin.cookapp.data.api

interface ApiCallback<T> {
    fun onSuccess(data: T)
    fun onError(message: String)
}