package ru.savushkin.cookapp.data.models

class Recipe(
    val uuid: String,
    val name: String,
    val images: ArrayList<String>,
    val description: String
)
